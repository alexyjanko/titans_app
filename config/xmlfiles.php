<?php
return [
    'employees' => [
        'xml' => env('EMPLOYEE_SOURCE_XML', 'employees.xml'),
        'xsd' => env('EMPLOYEE_SOURCE_XSD', 'employees.xsd'),
    ]
];
