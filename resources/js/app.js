import './bootstrap';
import 'bootstrap-icons/font/bootstrap-icons.css';
import {Modal} from "bootstrap";
import axios from "axios";

const employeeModal = new Modal(document.getElementById('employeeModal'))
const confirmModal = new Modal(document.getElementById('confirmModal'))
let confirmCallBack
let confirmParam

function confirmFunc(func, ...param) {
    confirmModal.toggle()
    confirmCallBack = func
    confirmParam = param
}

function confirmAccept() {
    confirmCallBack(confirmParam)
    confirmModal.toggle()
}

function openEditModal(id) {
    loadEmployeeData(id)
    openEmployeeModal(false)
}

async function loadEmployeeData(id) {
    const employee = employees.find((emp) => emp.id === id)
    Object.keys(employee).forEach((key) => setValue(`employee-${key}`, employee[key]))
}

function setValue(key, value) {
    const element = document.getElementById(key)
    if (!element) return
    element.value = value
}

function openEmployeeModal(reset = true) {
    if (reset) document.getElementById('employeeForm').reset()
    employeeModal.toggle()
}

async function saveEmployee() {
    const formData = new FormData(document.getElementById('employeeForm'))
    const id = formData.get('id')
    const employee = id ? await updateEmployee(id, formData) : await createEmployee(formData)
    const element = document.getElementById(employee.id)

    element ? updateRow(employee) : createRow(employee)
    employeeModal.toggle()
    makeChart()
}

async function updateEmployee(id, formData) {
    return axios.put('api/employee/' + id, formData).then((data) => data.data).catch((err) => alert(err.response.data.message))
}

function updateRow(employee) {
    attributes.forEach((key) => {
        const td = document.getElementById(employee.id + '-' + key)
        if (td) {
            td.innerHTML = employee[key]
        }
    })
    const i = employees.map((emp) => emp.id).indexOf(employee.id)
    employees[i] = employee;
    return employee
}

async function createEmployee(formData) {
    return axios.post('api/employee', formData).then((data) => data.data).catch((err) => alert(err.response.data.message))
}

function createRow(employee) {
    const tbody = document.getElementById('employeeTableBody')
    const tr = document.createElement('tr')
    tr.id = employee.id
    attributes.forEach((key) => {
        const td = document.createElement('td')
        if (key === 'id') return
        td.id = tr.id + '-' + key
        td.innerHTML = employee[key] ?? ""
        tr.appendChild(td)
    })
    const pencil = document.createElement('i')
    pencil.classList.add("bi", "bi-pencil-square")
    pencil.setAttribute('role', 'button')
    const trash = document.createElement('i')
    trash.classList.add("bi", "bi-trash3-fill")
    trash.setAttribute('role', 'button')

    const edit = document.createElement('td')
    edit.appendChild(pencil)

    edit.addEventListener('click', () => openEditModal(employee.id))
    const del = document.createElement('td')
    del.appendChild(trash)
    del.addEventListener('click', () => confirmFunc(deleteEmployee, employee.id))

    tr.appendChild(edit)
    tr.appendChild(del)
    tbody.appendChild(tr)
    employees.push(employee)
}

function deleteEmployee(id) {
    axios.delete('api/employee/' + id)
        .then((data) => removeElement(data.data))
        .catch((err) => alert(err.response.data.message))
    makeChart()
}

function removeElement(id) {
    document.getElementById(id).remove()
    const i = employees.map((emp) => emp.id).indexOf(id)
    delete employees[i]
    makeChart()
}

window.openEditModal = openEditModal
window.deleteEmployee = deleteEmployee
window.openEmployeeModal = openEmployeeModal
window.saveEmployee = saveEmployee
window.confirmFunc = confirmFunc
window.confirmAccept = confirmAccept
