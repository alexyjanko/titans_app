import Plotly from 'plotly.js-dist'
const chartDiv = document.getElementById('chart');

function toggleChart()
{
    makeChart()
    chartDiv.classList.toggle('d-none')
}
function makeChart() {
    employees.sort(function (a, b) {
        return a.age > b.age ? 1 : -1
    })
    const names = employees.map((emp) => emp.name + ' ' + emp.surname)
    const ages = employees.map((emp) => emp.age)

    Plotly.newPlot(
        chartDiv,
        [{
            type: "bar",
            x: names,
            y: ages
        }],
        {
            title: 'Employees - ages',
        }
    );
}

window.makeChart = makeChart
window.toggleChart = toggleChart
