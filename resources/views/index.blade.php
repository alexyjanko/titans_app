@php use Illuminate\Support\Js; @endphp
@extends('layouts.app')
@section('content')

    <script>
        var attributes = {{Js::from($attributes)}};
        var employees = {{Js::from($employees)}};
    </script>
    <div class="container">
        <button class="btn btn-success" onclick='toggleChart();'> Show Chart!</button>
        <div id="chart" class="d-none"></div>
        <table class="table table-striped">
            <thead>
            <tr>
                @foreach($attributes as $attribute)
                    <th>{{$attribute}}</th>
                @endforeach
                <th colspan="2">Actions</th>
            </tr>
            </thead>
            <tbody id="employeeTableBody">
            @foreach($employees as $employee)
                <tr id="{{$employee['id']}}">
                    @foreach($attributes as $attribute)
                        <td id="{{$employee['id']}}-{{$attribute}}">{{$employee[$attribute] ?? ''}}</td>
                    @endforeach
                    <td onclick="openEditModal('{{$employee['id']}}');">
                        <i role="button" class="bi bi-pencil-square"></i>

                    </td>
                    <td onclick="confirmFunc(deleteEmployee, '{{$employee['id']}}');">
                        <i role="button" class="bi bi-trash3-fill"></i>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div>
            <a class="btn btn-success"
               onclick="openEmployeeModal();">Add Employee</a>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="employeeModal" tabindex="-1" aria-labelledby="employeeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="employeeModalLabel">Edit Employee</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="employeeForm">
                        <input type="text" class="d-none" id="employee-id" name="id">
                        @foreach($attributes as $attribute)
                            <div class="mb-3">
                                <label for="recipient-name" class="col-form-label">{{$attribute}}</label>
                                <input type="text" class="form-control" id="employee-{{$attribute}}"
                                       name="{{$attribute}}">
                            </div>
                        @endforeach
                        <div class="btn btn-success"
                             onclick="saveEmployee()"
                        >Save
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="confirmModalLabel">Confirm</h5>
                </div>
                <div class="modal-body">
                    Are you sure?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="confirmAccept()">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection
