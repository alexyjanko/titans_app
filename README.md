## Requirements:
- php v8.1
- npm v.8.x
- run composer install
- run npm install
- run npm run build (or npm run dev for hot reload enviroment)
- run php artisan serve
- run cp .env.example .env

## Add new attributes to employees
- add new element to employeeInfo type in public/employees.xsd
- add new enum to App\Enums\EmployeeAttributeEnum with same string value as element in .xsd
- add validation rules for the enum in EmployeeAttributeEnum in validationRules() method
- new column will appear in table in webapp
