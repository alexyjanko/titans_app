<?php

use App\Http\Controllers\EmployeeController;
use Illuminate\Support\Facades\Route;

Route::prefix('employee')
    ->controller(EmployeeController::class)
    ->group(function () {
        Route::get('/{id}', 'show');
        Route::put('/{id}', 'update');
        Route::post('/', 'store');
        Route::delete('/{id}', 'delete');
});
