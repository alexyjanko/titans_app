<?php

namespace App\Http\Controllers;

use App\Enums\EmployeeAttributeEnum;
use App\Http\Resources\XMLResource;
use App\Repositories\EmployeeRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Request;

class EmployeeController extends Controller
{

    public function __construct(private readonly EmployeeRepository $employeeRepository)
    {
    }

    /**
     * @return Application|Factory|View
     */
    public function index(): View|Factory|Application
    {
        return view('index', [
            'employees' => XMLResource::fromMultiple($this->employeeRepository->all()),
            'attributes' => EmployeeAttributeEnum::casesValues(),
        ]);

    }

    /**
     * @param string|null $id
     * @return array
     */
    public function show(string $id = null): array
    {
        return XMLResource::fromOne($this->employeeRepository->get($id));
    }

    /**
     * @param string $id
     * @return array
     */
    public function update(string $id): array
    {
        $data = Request::validate(EmployeeAttributeEnum::validationRules());

        $employee = $this->employeeRepository->update($id, $data);

        return XMLResource::fromOne($employee);
    }

    /**
     * @return array
     */
    public function store(): array
    {
        $data = Request::validate(EmployeeAttributeEnum::validationRules());

        return XMLResource::fromOne($this->employeeRepository->create($data));
    }

    /**
     * @param string $id
     * @return string
     */
    public function delete(string $id): string
    {
        (new EmployeeRepository())->validate();

        return (new EmployeeRepository())->delete($id);
    }
}
