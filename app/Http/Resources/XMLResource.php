<?php

namespace App\Http\Resources;

use SimpleXMLElement;

class XMLResource
{
    /**
     * @param $input
     * @return array
     */
    public static function fromMultiple($input): array
    {
        $response = [];
        foreach ($input as $item) {
            $response[] = self::fromOne($item);
        }
        return $response;
    }

    /**
     * @param SimpleXMLElement $employee
     * @return array
     */
    public static function fromOne(SimpleXMLElement $employee): array
    {
        $resource = [];
        $resource['id'] = strval($employee->attributes()->id);
        foreach ($employee as $key => $rawValue) {
            $value = strval($rawValue);
            $resource[$key] = is_numeric($value) ? intval($value) : $value;
        }
        return $resource;
    }
}
