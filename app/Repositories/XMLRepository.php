<?php

namespace App\Repositories;

use DOMDocument;
use Exception;
use Illuminate\Support\Str;
use SimpleXMLElement;

class XMLRepository
{
    /** @var string $xmlNamespace */
    protected string $xmlNamespace = "app";
    /** @var string identifier of elements in xml */
    protected string $entityPrimaryKey = "id";
    /** @var string path to .xml storage file */
    protected string $xmlPath;
    /** @var string path to .xsd schema */
    protected string $xsdPath;
    /** @var string name of the main element in xml file */
    protected string $mainElement;

    /** @var SimpleXMLElement $simpleXml */
    protected SimpleXMLElement $simpleXml;

    /**
     * @throws Exception
     */
    public function __construct($config)
    {
        $this->xmlPath = public_path($config['xml']);
        $this->xsdPath = public_path($config['xsd']);
        if (!isset($this->mainElement)) {
            // if not set, set it from file name
            $this->mainElement = strtolower(last(explode('\\', $this::class)));
        }
        $this->load();
    }

    /**
     * load xml file into simple xml object
     * @return void
     * @throws Exception
     */
    public function load(): void
    {
        $this->simpleXml = new SimpleXMLElement(file_get_contents($this->xmlPath));
        $this->simpleXml->registerXPathNamespace("{$this->xmlNamespace}", 'https://www.w3schools.com');
    }

    /**
     * @return array|false|SimpleXMLElement[]|null
     */
    public function all(): bool|array|null
    {
        return $this->simpleXml->xpath("//{$this->xmlNamespace}:{$this->mainElement}");
    }

    /**
     * @param string $id
     * @return SimpleXMLElement
     */
    public function get(string $id): SimpleXMLElement
    {
        return $this->simpleXml
            ->xpath("//{$this->xmlNamespace}:{$this->mainElement}[@{$this->entityPrimaryKey}='{$id}']")[0];
    }

    /**
     * validate xml element and save it
     * @return void
     */
    public function save(): void
    {
        $this->validate();

        $this->simpleXml->saveXML($this->xmlPath);
    }

    /**
     * delete node from xml
     * @param string $id
     * @return string
     */
    public function delete(string $id): string
    {
        $node = $this->get($id);
        if (!empty($node)) {
            unset($node[0][0]);
        }
        $this->save();

        return $id;
    }

    /**
     * validate xml file
     * @return bool
     */
    public function validate()
    {
        $document = (new DOMDocument());
        $document->loadXML($this->simpleXml->asXML());

        return $document->schemaValidate($this->xsdPath);
    }

    /**
     * @param string $id
     * @param array $data
     * @return SimpleXMLElement
     */
    public function update(string $id, array $data): SimpleXMLElement
    {
        $element = $this->get($id);
        foreach ($data as $key => $value) {
            $element->{$key} = $value;
        }
        $this->save();

        return $element;
    }

    /**
     * create xml node
     * @param $data
     * @return SimpleXMLElement
     */
    public function create($data): SimpleXMLElement
    {
        $id = $this->mainElement . '-' . Str::random() . '-' . time();
        /** @var SimpleXMLElement $newNode */
        $newNode = $this->simpleXml->addChild($this->mainElement);
        $newNode->addAttribute('id', $id);
        foreach ($data as $key => $value) {
            $newNode->addChild($key, $value);

        }
        $this->save();

        return $newNode;
    }
}
