<?php

namespace App\Repositories;

class EmployeeRepository extends XMLRepository
{
    protected string $mainElement = 'employee';

    public function __construct()
    {
        parent::__construct(config('xmlfiles.employees'));
    }
}
