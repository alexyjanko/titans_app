<?php

namespace App\Enums;

enum EmployeeAttributeEnum: string
{
    case NAME = 'name';
    case SURNAME = 'surname';
    case AGE = 'age';

    /**
     * @return string[]
     */
    public static function validationRules(): array
    {
        return [
            self::NAME->value => 'required|string',
            self::SURNAME->value => 'required|string',
            self::AGE->value => 'required|numeric|gt:0',
        ];
    }
    public static function casesValues(): array
    {
        return array_column(self::cases(), 'value');
    }
}
